import csv
from math import sqrt
from random import randint
profiles = []
profiles.append((9, 2, 8, 9))
profiles.append((6, 7, 9, 7))
profiles.append((3, 8, 6, 3))
profiles.append((2, 3, 7, 8))
profiles.append((3, 4, 8, 8))

def import_file(name_file):
    """
    Importe un fichier csv

    Parameters
    ----------
    name_file : TYPE str

    Returns
    -------
    tab : TYPE liste de dictionnaires

    """
    with open (name_file, mode='r', encoding="utf-8") as f:
        reader = csv.DictReader(f, delimiter=';')
        tab = [{key: value for key, value in element.items()} for element in reader]
    return tab

def join_tab(tab_1, tab_2, base_joint):
    """
    Permet de fusionner deux tables

    Parameters
    ----------
    tab_1 : TYPE liste de dictionnaires
    tab_2 : TYPE liste de dictionnaires
    base_joint : TYPE str, désignant la clé 
                qui est à la base de la jointure des deux tables

    Returns
    -------
    tab_final : TYPE liste de dictionnaires

    """
    tab_final = []
    for element_1 in tab_1:
        for element_2 in tab_2:
            if element_1[base_joint] == element_2[base_joint]:
                element_1.update(element_2)
                tab_final.append(element_1)
    return tab_final

def cleaning_tab(tab):
    """
    Permet de convertir le type des valeurs de la table (de str à int)

    Parameters
    ----------
    tab : TYPE liste de dictionnaires.

    Returns
    -------
    tab : TYPE liste de dictionnaires

    """
    for element in tab:
        element['Courage'] = int(element['Courage'])
        element['Ambition'] = int(element['Ambition'])
        element['Intelligence'] = int(element['Intelligence'])
        element['Good'] = int(element['Good'])
    return tab

def calc_distance(cour, amb, intell, good, tab_base):
    """
    

    Parameters
    ----------
    cour : TYPE int
    amb : TYPE int
    intell : TYPE int.
    good : TYPE int
    tab_base : TYPE liste de dictionnaires

    Returns
    -------
    tab_base : TYPE liste de dictionnaires

    """
    for element in tab_base:
        element['Distance'] = sqrt((element['Courage'] - cour)**2 +
                                   (element['Ambition']- amb)**2 + 
                                   (element['Intelligence']- intell)**2 + 
                                   (element['Good']- good)**2)
    return tab_base

def sorting(tab):
    """
    Permet de ranger la table selon la valeur distance de chaque enregistrement

    Parameters
    ----------
    tab : TYPE liste de dictionnaire

    Returns
    -------
    tab : TYPE liste de dictionnaire rangé

    """
    for i in range(1, len(tab)):
        while tab[i]['Distance'] < tab[i - 1]['Distance'] and i > 0:
            tab[i], tab[i - 1] = tab[i - 1], tab[i]
            i = i - 1
    return tab

def finding_max(dictionnary):
    """
    Permet de trouver la clé à laquelle on associe valeur maximale 
    dans un dictionnaire

    Parameters
    ----------
    dictionnary : TYPE dictionnaire

    Returns
    -------
    estimated_key : TYPE int
    """
    maximum = 0
    for key, value in dictionnary.items():
        if value > maximum:
            maximum = value
            estimated_key = key
    return estimated_key

def knn(tab_sorted, k=5):
    """
    Permet de créer un table avec les plus proches voisins et leurs caractéristiques

    Parameters
    ----------
    tab_sorted : TYPE liste de dictionnaire
    k : TYPE int, optional, The default is 5.

    Returns
    -------
    table_neighbors : TYPE liste de dictionnaire des plus proches voisins
    """
    table_neighbors = []
    for i in range(k):
        table_neighbors.append(tab_sorted[i])
    return table_neighbors

def house_profil(tab_neighbors):
    """
    Permet de déterminer la maison du profil selon la maison des 
    plus proches voisins 

    Parameters
    ----------
    tab_neighbors : TYPE liste de dictionnaire des plus proches voisins

    Returns
    -------
    estimated_house : TYPE str

    """
    houses = {}
    for character in tab_neighbors:
        if character['House'] in houses:
            houses[character['House']] += 1
        else:
            houses[character['House']] = 1
    estimated_house = finding_max(houses)
    return estimated_house

def display_knn(tab_neighbors, cour, amb, intell, good, k=5):
    """
    Affichage des plus proches voisins et de la maison du profil saisi 

    Parameters
    ----------
    tab_neighbors : TYPE liste de dictionnaire des plus proches voisins
    cour : TYPE int
    amb : TYPE int
    intell : TYPE int
    good : TYPE int
    k : TYPE int, optional. The default is 5.

    Returns
    -------
    None.

    """
    print("Voici les caracteristiques du profil etudie: \n"
            f"- Courage : {cour} \n"\
            f"- Ambition : {amb} \n"\
            f"- Intelligence : {intell} \n"\
            f"- Good : {good}" )
    print(f"Les {k} plus proches voisins de ce profil sont:")
    for i in range(k):
        print(f"{i+1}) {tab_neighbors[i]['Name']} de la maison {tab_neighbors[i]['House']}")
    house = house_profil(tab_neighbors)
    print(f"La maison de ce profil selon l'algo des kppv est {house}")
    print("")

def creating_data_test(tab):
    """
    Création des données tests

    Parameters
    ----------
    tab : TYPE liste de dictionnaire 
    Returns
    -------
    characters_test : TYPE liste de dictionnaire 
    copy_characters : TYPE liste de dictionnaire 
    """
    characters_test = []
    copy_characters = tab[:]
    for _ in range(len(copy_characters) // 4):
        characters_test.append(copy_characters.pop(randint(0, len(copy_characters) - 1)))
    return characters_test, copy_characters

def cross_validation(tab, nb_tests=100):
    """
    Validation croisée permettant de tester l'efficacité de l'algorithme des kppv*
    tout en déterminant la meilleur valeur de k

    Parameters
    ----------
    tab_characters : TYPE liste de dictionnaire
    nb_tests : TYPE int optional. The default is 100.

    Returns
    -------
    best_k : TYPE int

    """
    percentages = {}
    for k in range(1, 20):
        bingo = 0
        for _ in range(nb_tests):
            characters_test, reference_characters = creating_data_test(tab)
            for character in characters_test:
                cour = character['Courage']
                amb = character['Ambition']
                intell = character['Intelligence']
                good = character['Good']
                reference_characters = calc_distance(cour, amb, intell, good, reference_characters)
                neighbors = sorting(reference_characters)
                if house_profil(neighbors[:k]) == character['House']:
                    bingo += 1
        percentages[k] = round(bingo / len(characters_test))
        print(f"Pourcentage de réussite avec k = {k} : {round(bingo / len(characters_test))} %")
    best_k = finding_max(percentages)
    return(best_k)

def ihm(tab, profiles):
    """
    Interface Homme Machine

    Parameters
    ----------
    tab : TYPE liste de dictionnaire
    profiles : TYPE liste de tuples

    Returns
    -------
    Message fin execution (str)

    """
    print("Bonjour, je suis le Choipeaux Magique!")
    reponse = int(input("Tapez 1 si vous souhaitez que je me prononce sur les profils automatiques (avec k=5) \n"\
            "Tapez 2 si vous voulez saisir manuellement des profils \n"\
            "Tapez 3 si vous souhaitez que je me prononce sur les profils avec une valeur de k de votre choix \n"\
            "Tapez 4 si vous souhaitez tester l'efficacité de l'algo tout en vous proposant la meilleure valeur de k: "))
    print("")
    if reponse == 1:
        for profile in profiles:
            print('')
            cour = profile[0]
            amb = profile[1]
            intell = profile[2]
            good = profile[3]
            tab_updated = calc_distance(cour, amb, intell, good, tab)
            tab_sorted = sorting(tab_updated)
            tab_neighbours = knn(tab_sorted)
            display_knn(tab_neighbours, cour, amb, intell, good)
            
    elif reponse == 2:
        amb = int(input("Saisissez une valeur caracterisant l'ambition de votre personnage: "))
        cour = int(input("Saisissez une valeur caracterisant le courage de votre personnage: "))
        intell = int(input("Saisissez une valeur caracterisant l'intelligence de votre personnage: "))
        good = int(input("Saisissez une valeur caracterisant la bonté de votre personnage: "))
        k = int(input("Choisissez une valeur pour k: "))
        tab_updated = calc_distance(cour, amb, intell, good, tab)
        tab_sorted = sorting(tab_updated)
        tab_neighbours = knn(tab_sorted, k)
        display_knn(tab_neighbours, cour, amb, intell, good, k)
        
    elif reponse == 3:
        new_k = int(input("Saisissez une valeur de k: "))
        for profile in profiles:
            print('')
            cour = profile[0]
            amb = profile[1]
            intell = profile[2]
            good = profile[3]
            tab_updated = calc_distance(cour, amb, intell, good, tab)
            tab_sorted = sorting(tab_updated)
            tab_neighbours = knn(tab_sorted, new_k)
            display_knn(tab_neighbours, cour, amb, intell, good, new_k)
    elif reponse == 4 :
        best_k = cross_validation(tab)
        print(f'La meilleur valeur de k est {best_k}')
    print('')
    
    relance_ihm = input("Veux-tu relancer l'ihm? \n\
        Entrer oui si vous voulez relancer le programme : ")
    reponse_possible = ["Oui", "oui", "OUI", "ou", "oi", "yes", "ye"]
    if relance_ihm not in reponse_possible:
        return("Fin execution")
    else:
        print('')
        return(ihm(tab, profiles))
        

tab_characteristics = import_file("Caracteristiques_des_persos.csv")
tab_characters = import_file('Characters.csv')
tab_characters_final = cleaning_tab(join_tab(tab_characters, tab_characteristics, "Name"))
print(ihm(tab_characters_final, profiles))

